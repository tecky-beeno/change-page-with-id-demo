import express from 'express'

let app = express()

app.use(express.static('public'))

app.get('/card/:id', (req, res) => {
  res.json({
    id: req.params.id,
    content: Math.random(),
  })
})

app.listen(8100)
